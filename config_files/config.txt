#database config parameters
host=localHost
user=sqlite
password=sqlite
DB=/tempFileDir/outputDir/wheat_phg_test.db
DBtype=sqlite

#Java arguments
Xmx=100G
numThreads=10

#Haplotype filtering
mapQ=48
DP_poisson_min=.01
DP_poisson_max=.99
GQ_min=50
filterHets=t

#sentieon license
sentieon_license=cbsulogin2.tc.cornell.edu:8990

#Consensus parameters
#Optional argument to get out merged VCF files for debugging consensus
exportMergedVCF=/tempFileDir/data/outputs/mergedVCFs/
includeVariants=false
mxDiv=.001
maxError=0.2
useDepth=true
replaceNsWithMajor=false

#FindPath Config parameters
maxNodesPerRange=30
minTaxaPerRange=1
minGBSReads=1
maxGBSReads=100
minTransitionProb=0.001
probReadMappedCorrectly=0.99
emissionMethod=allCounts
splitTaxa=false
